include .env

INV_DIR=ansible/inventory
BUILD_DIR=build

ANSIBLE=cd ansible && \
					ansible-playbook -i inventory \
						-e "service_user=${SERVICE_USER}" \
						-e "service_user_password=${SERVICE_USER_PASSWORD}"

STATE=terraform/terraform.tfstate

sync: sync-terraform sync-ansible

sync-terraform:
	@cd terraform && \
		terraform apply \
			-var linode_token=${LINODE_TOKEN} \
			-var root_password=${ROOT_PASSWORD}

sync-ansible: inventory aliases
	@${ANSIBLE} basis.yaml
	@${ANSIBLE} kubernetes.yaml

aliases:
	mkdir -p ${BUILD_DIR}
	cat ${STATE} | python3 scripts/filters/sanitize_tfstate.py | python3 scripts/generate_aliases.py > ${BUILD_DIR}/aliases
inventory:
	mkdir -p ${INV_DIR}
	cat ${STATE} | python3 scripts/filters/sanitize_tfstate.py | python3 scripts/filters/inventory_filter.py | \
		yq r - > ${INV_DIR}/inventory.yaml
generate-dotenv-file:
	@printf "\
		SERVICE_USER=\n\
		SERVICE_USER_PASSWORD=\n\
	  LINODE_TOKEN=\n\
		ROOT_PASSWORD=\n\
	">>.env

teardown:
	@cd terraform && \
		terraform destroy \
			-var linode_token=${LINODE_TOKEN} \
			-var root_password=${ROOT_PASSWORD}
	@rm -r ${INV_DIR} &2> /dev/null || true
	@rm -r ${BUILD_DIR} &2>/dev/null || true
purge: teardown
	@rm .env

	#@${ANSIBLE} glusterfs.yaml
test:
	@${ANSIBLE} kubernetes.yaml

