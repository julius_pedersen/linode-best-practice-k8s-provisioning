# Setting up Kubernetes on Linode

## Requirements
- Ansible
- Terraform
- Python3
- A Linode account with a service user/token

## Usage
1. Run `make create-dotenv-file` and populate it with values
2. Run `make sync-terraform` and wait for a minute
3. Run `make sync-ansible` and voila. K8s at your disposal

## Todo
A pretty TODO list can be found [here](https://www.notion.so/K8s-Cluster-6d6be33bb80842b2813cbff21af4e734)

## Working
* Provisions hardware on Linode
* Sets up a service user and adds it to sudoers
* Locks down sshd (no root ssh, key auth only)
* Installs K8s dependencies
* Sets up Kubernetes on the master node and connects workers to the master
* Sets up GlusterFS on all nodes and connects the master node to all worker nodes (only supports one master node for now)

## Bugs
* Terraform assumes the existence of a develish_rsa public key saved in Linode
* ufw sets something as changed even though nothing changed grrr
* CoreDNS doesn't like the firewall rules, so the firewall is disabled for now.
* make sync can not be used for provisioning, only validation. The Linode
Terraform provider returns finished too soon making playbooks fail because
the system is not yet ready
