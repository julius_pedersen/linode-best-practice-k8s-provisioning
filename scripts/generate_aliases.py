#!/usr/bin/python3

import json
import sys

data = json.loads(sys.stdin.read())
alias = list()

for host in data:
    ip = data[host]['public_ip']
    alias.append(f"alias {host}='ssh dsw@{ip}'")

print('\n'.join(alias))
