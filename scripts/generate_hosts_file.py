#!/usr/bin/python3

import json
import sys

data = json.loads(sys.stdin.read())
hosts = list()

for host in data:
    hosts.append(f'{data[host]["private_ip"]}\t\t{host}')

print('\n'.join(hosts))
