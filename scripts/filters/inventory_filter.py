#!/usr/bin/python3
"""
    Takes terraform.tfstate input and prints out a json ansible inventory
"""
import json
import sys

data = json.loads(sys.stdin.read())

masters = dict()
workers = dict()

for host in data:
    host_details = dict({
        'ansible_host': data[host]['public_ip'],
        'ansible_ssh_private_key_file': '~/.ssh/develish_rsa'
    })

    if 'master' in data[host]['tags']:
        masters[host] = host_details
    elif 'worker' in data[host]['tags']:
        workers[host] = host_details

inventory = {
    'all': {
        'children': {
            'masters': {'hosts': masters},
            'workers': {'hosts': workers}
        }
    }
}

print(json.dumps(inventory))
