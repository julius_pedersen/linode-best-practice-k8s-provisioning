#!/usr/bin/python3
"""
    Generates a GlusterFS topology file from sanitize_tfstate.py output
"""
import json
import sys

data = json.loads(sys.stdin.read())

nodes = list()
for host in data:
    nodes.append({
        "node": {
            "hostnames": {
                "manage": [host, ],
                "storage": [data[host]['private_ip']]
            },
            "devices": data[host]['volumes']
        }
    })

topology = dict({
    "clusters": [{"nodes": nodes}]
})

print(json.dumps(topology))
