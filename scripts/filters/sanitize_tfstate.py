#!/usr/bin/python3
"""
    Removes unneeded data from a terraform.tfstate file
    result:
    {
        hostname: {
            private_ip: string
            public_ip: string
            tags: list of strings
            volumes: list of strings (path to volume on node)
        }
    }
"""

import json
import sys

data = json.loads(sys.stdin.read())
hosts = dict()

resources_categories = dict()
for resource in data['resources']:
    if resource['type'] not in resources_categories:
        resources_categories[resource['type']] = list()

    resources_categories[resource['type']].append(resource)

nodes = list()
for resources_category in resources_categories['linode_instance']:
    for instance in resources_category['instances']:
        nodes.append(instance)

volumes = dict()
for resource_category in resources_categories['linode_volume']:
    for instance in resource_category['instances']:
        attrs = instance['attributes']

        if str(attrs['linode_id']) not in volumes:
            volumes[str(attrs['linode_id'])] = list()

        volumes[str(attrs['linode_id'])].append(attrs['filesystem_path'])

for node in nodes:
    attrs = node['attributes']

    host = dict(
        tags=attrs['tags'],
        public_ip=attrs['ip_address'],
        private_ip=attrs['private_ip_address'],
    )
    if attrs['id'] in volumes:
        host['volumes'] = volumes[attrs['id']]

    hosts[attrs['label']] = host

print(json.dumps(hosts))
