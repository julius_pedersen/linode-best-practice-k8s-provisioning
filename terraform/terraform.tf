variable "linode_token" {
  description = "Linode API token"
}

variable "root_password" {
  description = "The root password for all the nodes"
}

variable "label_prefix" {
  description = "Prepend all node labels with this"
  default = "develish"
}
