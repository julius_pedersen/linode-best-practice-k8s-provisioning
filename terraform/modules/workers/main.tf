resource "linode_instance" "linode_id" {
  count = var.nodes

  image = "linode/debian10"
  label = "${var.label_prefix}${count.index + 1}worker"
  tags = ["k8s", "worker"]
  group = "k8s"
  region = var.region
  type = var.linode_type
  private_ip = true

  swap_size = 512
  authorized_keys = [ data.linode_sshkey.develish_rsa.ssh_key ]
  root_pass = var.root_pass
}

resource "linode_volume" "linode_volume" {
  count = length(linode_instance.linode_id)

  label = "${var.label_prefix}${count.index + 1}workerbrick"
  region = var.region
  linode_id = linode_instance.linode_id[count.index].id
  size = 10
}
