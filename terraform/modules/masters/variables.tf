
variable "nodes" {
  description = "The number of nodes to create"
  default = 1
}

variable "label_prefix" {
  description = "Prepend the node label with this"
}
variable "region" {
  description = "The region to where we provision the linodes"
  default = "eu-central"
}
variable "linode_type" {
  description = "The type of linode to provision"
  default = "g6-standard-2"
}

variable "root_password" {
  description = "The root password of the node"
}
