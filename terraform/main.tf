provider "linode" {
  version = "~> 1.8"
  token = var.linode_token
}

module "masters" {
  source = "./modules/masters"
  nodes  = 1

  label_prefix = var.label_prefix
  root_password = var.root_password
}

module "workers" {
  source = "./modules/workers"
  nodes  = 2

  label_prefix = var.label_prefix
  root_pass = var.root_password
}
